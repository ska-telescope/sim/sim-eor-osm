# The scripts for CSD3

## Separate simulation and imaging SLURM jobs
The simulations and image construction can be performed using a single GPU node with up to 4 GPUs.
Two components of OSKAR package are used to make simulations, `oskar_sim_interferometer` and `oskar_imager` .
The example SLURM scripts for both components are 
`slurm_oskar_sim_interferometer_csd3.sh` and  `slurm_oskar_imager_csd3.sh` .

## Joint simulation and imaging SLURM jobs
To reduce the number of separate SLURM jobs, a shell script `sim_EoR.sh` is used to start both visibility
simulations and dirty image construction. The example SLURM scripts to run simulations for EoR0, EoR1 and EoR2 fields are
```
slurm_sim_EoR0_20deg_csd3.sh
slurm_sim_EoR1_20deg_csd3.sh
slurm_sim_EoR2_20deg_csd3.sh
```

All `*.ini` files are the parameter files for the corresponding OSKAR components.

The initial example scripts are provided by Fred Dulwich.
